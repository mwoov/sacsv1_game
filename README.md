# README

This is the videogame implementation for the game **Space Academy: Combat Simulator V.1.** A _shoot 'em up_ videogame where the player controls a spaceship through an infinite map killing enemies on the go. All the player's scores will be summited to a cloud system, where other players can compare theirs and check the leaderboards for the best scores registered.

## Dev tools

- `Unity 5.5.2f1`

## Getting started

To play the game you must compile an _Android build_ doing the following:

- On `Unity` choose: _File > Build Settings_
- Make sure that _Android_ is the current platform selected.
- Click on _Build_.
- Install the _.apk_ generated on an Android device.
- (Hope that everything is up and running fine).
- Enjoy the game!